import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockdataCreateComponent } from './stockdata-create/stockdata-create.component';
import { StockdataEditComponent } from './stockdata-edit/stockdata-edit.component';
import { StockdataListComponent } from './stockdata-list/stockdata-list.component';
import { PageStylingComponent } from './shared/page-styling/page-styling.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { MarketComponent} from './market/market.component';
import { TradeComponent} from './trade/trade.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { PiesComponent } from './pies/pies.component';
import { TradeFormComponent } from './trade-form/trade-form.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomeComponent},
  { path: 'market', component: MarketComponent},
  { path: 'trade', component: TradeComponent},
  { path: 'portfolio', component: PortfolioComponent},
  { path: 'stockdata-create', component: StockdataCreateComponent },
  { path: 'stockdata-list', component: StockdataListComponent },
  { path: 'stockdata-edit/:id', component: StockdataEditComponent },
  { path: 'pies', component: PiesComponent},
  { path: 'trade-form', component: TradeFormComponent},
  { path: '**', component: StockdataListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
