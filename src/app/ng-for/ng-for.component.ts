import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-for',
  templateUrl: './ng-for.component.html',
  styleUrls: ['./ng-for.component.css']
})
export class NgForComponent {

  id: number;
  stockTicker: string;
  price: number;
  volume: number;
  buyOrSell: string;
  statusCode: number;

  constructor() {
    this.id = 2;
    this.stockTicker = 'AAPL';
    this.price = 352.40;
    this.volume = 47828974;
    this.buyOrSell = 'SELL';
    this.statusCode = 200;
  }


}

