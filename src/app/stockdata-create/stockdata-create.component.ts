import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-stockdata-create',
  templateUrl: './stockdata-create.component.html',
  styleUrls: ['./stockdata-create.component.css']
})
export class StockdataCreateComponent implements OnInit {

  @Input() stockdataDetails = { id: 0, stockTicker: '', price: 0, volume: 0, act: '', statusCode: 0, date: new Date()}

  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  addStockdata() {
    this.restApi.createStockdata(this.stockdataDetails).subscribe( (data: {}) => {
      this.router.navigate(['/stockdata-list'])
    })
  }

}
