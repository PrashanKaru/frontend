import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockdataCreateComponent } from './stockdata-create.component';

describe('StockdataCreateComponent', () => {
  let component: StockdataCreateComponent;
  let fixture: ComponentFixture<StockdataCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockdataCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockdataCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
