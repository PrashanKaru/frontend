import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StockdataCreateComponent } from './stockdata-create/stockdata-create.component';
import { StockdataListComponent } from './stockdata-list/stockdata-list.component';
import { StockdataEditComponent } from './stockdata-edit/stockdata-edit.component';
import { NgForComponent } from './ng-for/ng-for.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { PageStylingComponent } from './shared/page-styling/page-styling.component';
import { HomeComponent } from './home/home.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { MarketComponent } from './market/market.component';
import { TradeComponent } from './trade/trade.component';
import { PiesComponent } from './pies/pies.component';

//design imports
import { NgxEchartsModule } from 'ngx-echarts';
import { TradeFormComponent } from './trade-form/trade-form.component';


@NgModule({
  declarations: [
    AppComponent,
    StockdataCreateComponent,
    StockdataListComponent,
    StockdataEditComponent,
    NgForComponent,
    NavbarComponent,
    PageStylingComponent,
    HomeComponent,
    PortfolioComponent,
    MarketComponent,
    TradeComponent,
    PiesComponent,
    TradeFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxEchartsModule.forRoot({
      /**
       * This will import all modules from echarts.
       * If you only need custom modules,
       * please refer to [Custom Build] section.
       */
      echarts: () => import('echarts'), // or import('./path-to-my-custom-echarts')
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
