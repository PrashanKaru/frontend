import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { EChartsOption } from 'echarts';

@Component({
  selector: 'app-pies',
  templateUrl: './pies.component.html',
  styleUrls: ['./pies.component.css'],
})
export class PiesComponent implements OnInit {
  chartOption: EChartsOption = {
    series: [
      {
        type: 'pie',
        data: [
          // Example data format
          // {
          //   value: 335,
          //   name: 'Direct Visit'
          // },
          // {
          //   value: 234,
          //   name: 'Union Ad'
          // }
        ],
        radius: '50%',
      },
    ],
  };

  constructor(public restApi: RestApiService) {}

  ngOnInit(): void {
    this.restApi.getPortfolioPie().subscribe((data: {}) => {
      this.chartOption.series = [
        { type: 'pie', data: JSON.parse(JSON.stringify(data)), radius: '50%' },
      ];
    });
  }
}
