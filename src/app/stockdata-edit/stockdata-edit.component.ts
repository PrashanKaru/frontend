
import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-stockdata-edit',
  templateUrl: './stockdata-edit.component.html',
  styleUrls: ['./stockdata-edit.component.css']
})
export class StockdataEditComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  stockdataDetails: any = {};
  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.restApi.getStockdataById(this.id).subscribe((data: {}) => {
      this.stockdataDetails = data;
    })
  }

  updateStockdata() {
    if(window.confirm('Are you sure, you want to update?')){
      this.restApi.updateStockdata(this.id, this.stockdataDetails).subscribe(data => {
        this.router.navigate(['/stockdata-list'])
      })
    }
  }

}
