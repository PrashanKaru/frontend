import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockdataEditComponent } from './stockdata-edit.component';

describe('StockdataEditComponent', () => {
  let component: StockdataEditComponent;
  let fixture: ComponentFixture<StockdataEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockdataEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockdataEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
