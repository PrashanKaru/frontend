export class Stockdata {
  constructor() {
    this.id = 1;
    this.stockTicker = '';
    this.price = 0;
    this.volume = 0;
    this.act = '';
    this.statusCode = 200;
    this.date = new Date();
  }

  id: number;
  stockTicker: string;
  price: number;
  volume: number;
  act: string;
  statusCode: number;
  date: Date;
}
