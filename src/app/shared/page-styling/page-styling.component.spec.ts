import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageStylingComponent } from './page-styling.component';

describe('PageStylingComponent', () => {
  let component: PageStylingComponent;
  let fixture: ComponentFixture<PageStylingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageStylingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageStylingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
