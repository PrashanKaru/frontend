import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Stockdata } from '../shared/stockdata';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RestApiService {
  //apiURL = environment.apiurl;
  apiURL ='http://springfield2-springfield2.emeadocker45.conygre.com/api/v1/stockdata';
  // testURL = 'http://localhost:8080/api/v1/stockdata/portfolio-pie';

  constructor(private http: HttpClient) {}

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  // HttpClient API get() method
  getStockdata() {
    return this.http
      .get<Stockdata>(this.apiURL)
      .pipe(retry(1), catchError(this.handleError));
  }
  // HttpClient API getById() method
  getStockdataById(id: any): Observable<Stockdata> {
    return this.http
      .get<Stockdata>(this.apiURL + '/' + id)
      .pipe(retry(1), catchError(this.handleError));
  }

  // GET portfolio pie
  getPortfolioPie(): Observable<JSON> {
    return this.http
      .get<JSON>(this.apiURL + '/portfolio-pie')
      .pipe(retry(1), catchError(this.handleError));
  }

  // HttpClient API get() method
  setStockTicker(id: any): Observable<Stockdata> {
    return this.http
      .get<Stockdata>(this.apiURL + id)
      .pipe(retry(1), catchError(this.handleError));
  }

  // HttpClient API post() method
  createStockdata(stockdata: Stockdata): Observable<Stockdata> {
    return this.http
      .post<Stockdata>(
        this.apiURL + '',
        JSON.stringify(stockdata),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  // HttpClient API put() method
  updateStockdata(id: number, stockdata: Stockdata): Observable<Stockdata> {
    return this.http
      .put<Stockdata>(
        this.apiURL + '',
        JSON.stringify(stockdata),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  // HttpClient API delete() method
  deleteStockdata(id: number) {
    return this.http
      .delete<Stockdata>(this.apiURL + '/' + id, this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }

  // Error handling
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
