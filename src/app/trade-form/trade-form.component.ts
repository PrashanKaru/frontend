import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-trade-form',
  templateUrl: './trade-form.component.html',
  styleUrls: ['./trade-form.component.css']
})
export class TradeFormComponent implements OnInit {
  stockdataDetails: any = {};
  id = this.actRoute.snapshot.params['id'];
  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }

  buyStock() {
    if(window.confirm('Are you sure, you want to buy?')){
      this.restApi.updateStockdata(this.id, this.stockdataDetails).subscribe(data => {
        this.router.navigate(['/stockdata-list'])
      })
    }
  }

  ngOnInit(): void {
  }

}




