import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockdataListComponent } from './stockdata-list.component';

describe('StockdataListComponent', () => {
  let component: StockdataListComponent;
  let fixture: ComponentFixture<StockdataListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockdataListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockdataListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
