
import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-stockdata-list',
  templateUrl: './stockdata-list.component.html',
  styleUrls: ['./stockdata-list.component.css']
})
export class StockdataListComponent implements OnInit {

  Stockdata: any = [];
  constructor(
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadStockdata()
  }

  loadStockdata() {
    return this.restApi.getStockdata().subscribe((data: {}) => {
        this.Stockdata = data;
    })
  }

  deleteStockdata(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteStockdata(id).subscribe(data => {
        this.loadStockdata()
      })
    }
  }

}

